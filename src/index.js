import fallbackThumb from './fallback.png';
import './index.scss';

export default class DragDrop {
  constructor(itemContainer, dropzone, options) {
    this.mouseMove = this.mouseMove.bind(this);
    this.mouseDown = this.mouseDown.bind(this);
    this.mouseUp = this.mouseUp.bind(this);
    this.childrenChanged = this.childrenChanged.bind(this);
    this.update = this.update.bind(this);

    this.element = itemContainer;
    this.dropzone = dropzone;

    this.options = {
      preload: false, // if true, thumbnails will be preloaded instead of when needed
      selector: null,
      centered: true, // thumbnails will be centered to cursor
      offset: { x: 0, y: 0 },
      size: { width: 100, height: 100 },
      scrollMargin: 20,
      scrollAmount: 10,
      scrollInterval: 50,
      autoListen: true, // if false user must manually turn the listeners on and off with listen / unlisten methods
      disableContextMenu: false,
    };

    Object.assign(this.options, options);

    this.state = {
      dragging: false,
      current: null,
      position: { x: 0, y: 0 },
      listening: false,
    };

    this.thumbs = document.createElement('div');
    this.thumbs.className = 'dragdrop-thumbs';

    this.element.classList.add('dragdrop-container');

    this.observer = new MutationObserver(this.childrenChanged);
    this.observer.observe(this.element, { childList: true });

    if (this.options.autoListen) this.listen();
    this.update();
  }

  childrenChanged(changes) {
    if (!this.state.listening) return;

    changes.forEach((change) => {
      change.addedNodes.forEach((added) => {
        this.addItem(added);
      });

      change.removedNodes.forEach((removed) => {
        this.removeItem(removed);
      });
    });
  }

  listen() {
    this.state.listening = true;
    window.addEventListener('pointermove', this.mouseMove);
    window.addEventListener('pointerup', this.mouseUp);
    window.addEventListener('pointerleave', this.mouseUp);
    this.element.addEventListener('touchmove', this.touchMove);

    this.state.items = Array.from(this.element.children);
    this.state.items.forEach((item) => {
      this.addItem(item);
    });
  }

  unlisten() {
    this.state.listening = false;
    window.removeEventListener('pointermove', this.mouseMove);
    window.removeEventListener('pointerup', this.mouseUp);
    window.removeEventListener('pointerleave', this.mouseUp);
    this.element.removeEventListener('touchmove', this.touchMove);

    this.state.items = Array.from(this.element.children);
    this.state.items.forEach((item) => {
      this.removeItem(item);
    });
  }

  addItem(_item) {
    const item = this.options.selector ? _item.querySelector(this.options.selector) : _item;
    const thumb = item.getAttribute('data-thumb');
    if (!thumb) {
      console.warn(`ds-dragdrop warning: item ${item} missing 'data-thumb' attribute`);
    }
    item.addEventListener('dragstart', this.dragStart);
    item.addEventListener('pointerdown', this.mouseDown);
    if (thumb && this.options.preload) {
      const img = new Image();
      img.src = thumb;
    }
  }

  removeItem(_item) {
    const item = this.options.selector ? _item.querySelector(this.options.selector) : _item;
    item.removeEventListener('dragstart', this.dragStart);
    item.removeEventListener('pointerdown', this.mouseDown);
  }

  dragStart(e) {
    e.preventDefault();
  }

  mouseDown(e) {
    const path = e.path || (e.composedPath && e.composedPath());
    if (path[0].getAttribute('draggable') === 'false') return;
    // get target, making sure not to accidentally grab a child of the intended draggable item
    let { target } = e;
    while (target.parentNode !== this.element) {
      target = target.parentNode;
    }

    this.state.dragging = true;

    const thumb = new Image();
    thumb.style.width = `${this.options.size.width}px`;
    thumb.style.height = `${this.options.size.height}px`;
    thumb.src = target.getAttribute('data-thumb') || fallbackThumb;
    thumb.className = 'dragdrop-thumb';
    thumb.setAttribute('draggable', false);
    if (this.options.disableContextMenu) {
      thumb.oncontextmenu = () => { return false };
    }
    document.body.appendChild(thumb);
    this.state.current = target;
    this.state.thumb = thumb;
    this.moveThumbToCursor(e);
    this.state.startDrag = true;
  }

  mouseMove(e) {
    if (this.state.startDrag) {
      if (this.state.current) this.element.dispatchEvent(new CustomEvent('dragged', { detail: this.state.current }));

      this.state.startDrag = false;
    }
    this.state.position.x = e.clientX;
    this.state.position.y = e.clientY;
    if (this.state.dragging) {
      this.moveThumbToCursor(e);
    }
  }

  touchMove(e) {
    e.preventDefault();
  }

  mouseUp(e) {
    this.state.dragging = false;
    const { left, top, width, height } = this.dropzone.getBoundingClientRect();

    const inDropZone = (e.clientY > top && e.clientY < top + height) && (e.clientX > left && e.clientX < left + width);

    if (this.state.current) {
      this.element.dispatchEvent(new CustomEvent('dropped', { detail: {
        inDropZone,
        element: this.state.current,
        position: this.state.position,
      } }));
      this.state.thumb.remove();
    }
    this.state.current = null;
  }

  moveThumbToCursor(e) {
    this.state.thumb.style.left = `${e.clientX + this.options.offset.x}px`;
    this.state.thumb.style.top = `${e.clientY + this.options.offset.y}px`;
  }


  update() {
    if (this.state.dragging) {
      if (this.state.position.y < this.options.scrollMargin) {
        window.scrollBy(0, -this.options.scrollAmount);
      }
      if (window.innerHeight - this.state.position.y < this.options.scrollMargin) {
        window.scrollBy(0, this.options.scrollAmount);
      }
    }
    setTimeout(this.update, this.options.scrollInterval);
  }
}

window.DragDrop = DragDrop;
