(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["ds-dragdrop"] = factory();
	else
		root["ds-dragdrop"] = factory();
})(self, function() {
return /******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/css-loader/index.js!./node_modules/sass-loader/dist/cjs.js!./src/index.scss":
/*!**************************************************************************************************!*\
  !*** ./node_modules/css-loader/index.js!./node_modules/sass-loader/dist/cjs.js!./src/index.scss ***!
  \**************************************************************************************************/
/***/ ((module, exports, __webpack_require__) => {

exports = module.exports = __webpack_require__(/*! ../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.id, ".dragdrop-thumb {\n  position: fixed;\n  transform: translate(-50%, -50%); }\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/lib/css-base.js":
/*!*************************************************!*\
  !*** ./node_modules/css-loader/lib/css-base.js ***!
  \*************************************************/
/***/ ((module) => {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ "./src/index.scss":
/*!************************!*\
  !*** ./src/index.scss ***!
  \************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


var content = __webpack_require__(/*! !!../node_modules/css-loader/index.js!../node_modules/sass-loader/dist/cjs.js!./index.scss */ "./node_modules/css-loader/index.js!./node_modules/sass-loader/dist/cjs.js!./src/index.scss");

if(typeof content === 'string') content = [[module.id, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! !../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/lib/addStyles.js":
/*!****************************************************!*\
  !*** ./node_modules/style-loader/lib/addStyles.js ***!
  \****************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/

var stylesInDom = {};

var	memoize = function (fn) {
	var memo;

	return function () {
		if (typeof memo === "undefined") memo = fn.apply(this, arguments);
		return memo;
	};
};

var isOldIE = memoize(function () {
	// Test for IE <= 9 as proposed by Browserhacks
	// @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
	// Tests for existence of standard globals is to allow style-loader
	// to operate correctly into non-standard environments
	// @see https://github.com/webpack-contrib/style-loader/issues/177
	return window && document && document.all && !window.atob;
});

var getTarget = function (target, parent) {
  if (parent){
    return parent.querySelector(target);
  }
  return document.querySelector(target);
};

var getElement = (function (fn) {
	var memo = {};

	return function(target, parent) {
                // If passing function in options, then use it for resolve "head" element.
                // Useful for Shadow Root style i.e
                // {
                //   insertInto: function () { return document.querySelector("#foo").shadowRoot }
                // }
                if (typeof target === 'function') {
                        return target();
                }
                if (typeof memo[target] === "undefined") {
			var styleTarget = getTarget.call(this, target, parent);
			// Special case to return head of iframe instead of iframe itself
			if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
				try {
					// This will throw an exception if access to iframe is blocked
					// due to cross-origin restrictions
					styleTarget = styleTarget.contentDocument.head;
				} catch(e) {
					styleTarget = null;
				}
			}
			memo[target] = styleTarget;
		}
		return memo[target]
	};
})();

var singleton = null;
var	singletonCounter = 0;
var	stylesInsertedAtTop = [];

var	fixUrls = __webpack_require__(/*! ./urls */ "./node_modules/style-loader/lib/urls.js");

module.exports = function(list, options) {
	if (typeof DEBUG !== "undefined" && DEBUG) {
		if (typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};

	options.attrs = typeof options.attrs === "object" ? options.attrs : {};

	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (!options.singleton && typeof options.singleton !== "boolean") options.singleton = isOldIE();

	// By default, add <style> tags to the <head> element
        if (!options.insertInto) options.insertInto = "head";

	// By default, add <style> tags to the bottom of the target
	if (!options.insertAt) options.insertAt = "bottom";

	var styles = listToStyles(list, options);

	addStylesToDom(styles, options);

	return function update (newList) {
		var mayRemove = [];

		for (var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];

			domStyle.refs--;
			mayRemove.push(domStyle);
		}

		if(newList) {
			var newStyles = listToStyles(newList, options);
			addStylesToDom(newStyles, options);
		}

		for (var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];

			if(domStyle.refs === 0) {
				for (var j = 0; j < domStyle.parts.length; j++) domStyle.parts[j]();

				delete stylesInDom[domStyle.id];
			}
		}
	};
};

function addStylesToDom (styles, options) {
	for (var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];

		if(domStyle) {
			domStyle.refs++;

			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}

			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];

			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}

			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles (list, options) {
	var styles = [];
	var newStyles = {};

	for (var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = options.base ? item[0] + options.base : item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};

		if(!newStyles[id]) styles.push(newStyles[id] = {id: id, parts: [part]});
		else newStyles[id].parts.push(part);
	}

	return styles;
}

function insertStyleElement (options, style) {
	var target = getElement(options.insertInto)

	if (!target) {
		throw new Error("Couldn't find a style target. This probably means that the value for the 'insertInto' parameter is invalid.");
	}

	var lastStyleElementInsertedAtTop = stylesInsertedAtTop[stylesInsertedAtTop.length - 1];

	if (options.insertAt === "top") {
		if (!lastStyleElementInsertedAtTop) {
			target.insertBefore(style, target.firstChild);
		} else if (lastStyleElementInsertedAtTop.nextSibling) {
			target.insertBefore(style, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			target.appendChild(style);
		}
		stylesInsertedAtTop.push(style);
	} else if (options.insertAt === "bottom") {
		target.appendChild(style);
	} else if (typeof options.insertAt === "object" && options.insertAt.before) {
		var nextSibling = getElement(options.insertAt.before, target);
		target.insertBefore(style, nextSibling);
	} else {
		throw new Error("[Style Loader]\n\n Invalid value for parameter 'insertAt' ('options.insertAt') found.\n Must be 'top', 'bottom', or Object.\n (https://github.com/webpack-contrib/style-loader#insertat)\n");
	}
}

function removeStyleElement (style) {
	if (style.parentNode === null) return false;
	style.parentNode.removeChild(style);

	var idx = stylesInsertedAtTop.indexOf(style);
	if(idx >= 0) {
		stylesInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement (options) {
	var style = document.createElement("style");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}

	if(options.attrs.nonce === undefined) {
		var nonce = getNonce();
		if (nonce) {
			options.attrs.nonce = nonce;
		}
	}

	addAttrs(style, options.attrs);
	insertStyleElement(options, style);

	return style;
}

function createLinkElement (options) {
	var link = document.createElement("link");

	if(options.attrs.type === undefined) {
		options.attrs.type = "text/css";
	}
	options.attrs.rel = "stylesheet";

	addAttrs(link, options.attrs);
	insertStyleElement(options, link);

	return link;
}

function addAttrs (el, attrs) {
	Object.keys(attrs).forEach(function (key) {
		el.setAttribute(key, attrs[key]);
	});
}

function getNonce() {
	if (false) {}

	return __webpack_require__.nc;
}

function addStyle (obj, options) {
	var style, update, remove, result;

	// If a transform function was defined, run it on the css
	if (options.transform && obj.css) {
	    result = typeof options.transform === 'function'
		 ? options.transform(obj.css) 
		 : options.transform.default(obj.css);

	    if (result) {
	    	// If transform returns a value, use that instead of the original css.
	    	// This allows running runtime transformations on the css.
	    	obj.css = result;
	    } else {
	    	// If the transform function returns a falsy value, don't add this css.
	    	// This allows conditional loading of css
	    	return function() {
	    		// noop
	    	};
	    }
	}

	if (options.singleton) {
		var styleIndex = singletonCounter++;

		style = singleton || (singleton = createStyleElement(options));

		update = applyToSingletonTag.bind(null, style, styleIndex, false);
		remove = applyToSingletonTag.bind(null, style, styleIndex, true);

	} else if (
		obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function"
	) {
		style = createLinkElement(options);
		update = updateLink.bind(null, style, options);
		remove = function () {
			removeStyleElement(style);

			if(style.href) URL.revokeObjectURL(style.href);
		};
	} else {
		style = createStyleElement(options);
		update = applyToTag.bind(null, style);
		remove = function () {
			removeStyleElement(style);
		};
	}

	update(obj);

	return function updateStyle (newObj) {
		if (newObj) {
			if (
				newObj.css === obj.css &&
				newObj.media === obj.media &&
				newObj.sourceMap === obj.sourceMap
			) {
				return;
			}

			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;

		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag (style, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (style.styleSheet) {
		style.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = style.childNodes;

		if (childNodes[index]) style.removeChild(childNodes[index]);

		if (childNodes.length) {
			style.insertBefore(cssNode, childNodes[index]);
		} else {
			style.appendChild(cssNode);
		}
	}
}

function applyToTag (style, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		style.setAttribute("media", media)
	}

	if(style.styleSheet) {
		style.styleSheet.cssText = css;
	} else {
		while(style.firstChild) {
			style.removeChild(style.firstChild);
		}

		style.appendChild(document.createTextNode(css));
	}
}

function updateLink (link, options, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	/*
		If convertToAbsoluteUrls isn't defined, but sourcemaps are enabled
		and there is no publicPath defined then lets turn convertToAbsoluteUrls
		on by default.  Otherwise default to the convertToAbsoluteUrls option
		directly
	*/
	var autoFixUrls = options.convertToAbsoluteUrls === undefined && sourceMap;

	if (options.convertToAbsoluteUrls || autoFixUrls) {
		css = fixUrls(css);
	}

	if (sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = link.href;

	link.href = URL.createObjectURL(blob);

	if(oldSrc) URL.revokeObjectURL(oldSrc);
}


/***/ }),

/***/ "./node_modules/style-loader/lib/urls.js":
/*!***********************************************!*\
  !*** ./node_modules/style-loader/lib/urls.js ***!
  \***********************************************/
/***/ ((module) => {


/**
 * When source maps are enabled, `style-loader` uses a link element with a data-uri to
 * embed the css on the page. This breaks all relative urls because now they are relative to a
 * bundle instead of the current page.
 *
 * One solution is to only use full urls, but that may be impossible.
 *
 * Instead, this function "fixes" the relative urls to be absolute according to the current page location.
 *
 * A rudimentary test suite is located at `test/fixUrls.js` and can be run via the `npm test` command.
 *
 */

module.exports = function (css) {
  // get current location
  var location = typeof window !== "undefined" && window.location;

  if (!location) {
    throw new Error("fixUrls requires window.location");
  }

	// blank or null?
	if (!css || typeof css !== "string") {
	  return css;
  }

  var baseUrl = location.protocol + "//" + location.host;
  var currentDir = baseUrl + location.pathname.replace(/\/[^\/]*$/, "/");

	// convert each url(...)
	/*
	This regular expression is just a way to recursively match brackets within
	a string.

	 /url\s*\(  = Match on the word "url" with any whitespace after it and then a parens
	   (  = Start a capturing group
	     (?:  = Start a non-capturing group
	         [^)(]  = Match anything that isn't a parentheses
	         |  = OR
	         \(  = Match a start parentheses
	             (?:  = Start another non-capturing groups
	                 [^)(]+  = Match anything that isn't a parentheses
	                 |  = OR
	                 \(  = Match a start parentheses
	                     [^)(]*  = Match anything that isn't a parentheses
	                 \)  = Match a end parentheses
	             )  = End Group
              *\) = Match anything and then a close parens
          )  = Close non-capturing group
          *  = Match anything
       )  = Close capturing group
	 \)  = Match a close parens

	 /gi  = Get all matches, not the first.  Be case insensitive.
	 */
	var fixedCss = css.replace(/url\s*\(((?:[^)(]|\((?:[^)(]+|\([^)(]*\))*\))*)\)/gi, function(fullMatch, origUrl) {
		// strip quotes (if they exist)
		var unquotedOrigUrl = origUrl
			.trim()
			.replace(/^"(.*)"$/, function(o, $1){ return $1; })
			.replace(/^'(.*)'$/, function(o, $1){ return $1; });

		// already a full url? no change
		if (/^(#|data:|http:\/\/|https:\/\/|file:\/\/\/|\s*$)/i.test(unquotedOrigUrl)) {
		  return fullMatch;
		}

		// convert the url to a full url
		var newUrl;

		if (unquotedOrigUrl.indexOf("//") === 0) {
		  	//TODO: should we add protocol?
			newUrl = unquotedOrigUrl;
		} else if (unquotedOrigUrl.indexOf("/") === 0) {
			// path should be relative to the base url
			newUrl = baseUrl + unquotedOrigUrl; // already starts with '/'
		} else {
			// path should be relative to current directory
			newUrl = currentDir + unquotedOrigUrl.replace(/^\.\//, ""); // Strip leading './'
		}

		// send back the fixed url(...)
		return "url(" + JSON.stringify(newUrl) + ")";
	});

	// send back the fixed css
	return fixedCss;
};


/***/ }),

/***/ "./src/fallback.png":
/*!**************************!*\
  !*** ./src/fallback.png ***!
  \**************************/
/***/ ((module) => {

"use strict";
module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAfQAAAH0CAYAAADL1t+KAAAACXBIWXMAAAsTAAALEwEAmpwYAAAGv2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNi4wLWMwMDYgNzkuZGFiYWNiYiwgMjAyMS8wNC8xNC0wMDozOTo0NCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIDIyLjQgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAyMS0wOS0yNVQxMjoyMTo1Ny0wNDowMCIgeG1wOk1vZGlmeURhdGU9IjIwMjEtMDktMjdUMTE6MDI6MzQtMDQ6MDAiIHhtcDpNZXRhZGF0YURhdGU9IjIwMjEtMDktMjdUMTE6MDI6MzQtMDQ6MDAiIGRjOmZvcm1hdD0iaW1hZ2UvcG5nIiBwaG90b3Nob3A6Q29sb3JNb2RlPSIzIiBwaG90b3Nob3A6SUNDUHJvZmlsZT0ic1JHQiBJRUM2MTk2Ni0yLjEiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OWU5ZGI5Y2EtZTkyMi1kYjQ3LTg3OTgtOWUxMzMxNGNiZThjIiB4bXBNTTpEb2N1bWVudElEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6ZDRkNmU1NDEtYjQ5Yi0wOTRhLWJlNmQtODUwYTJiZDA1MGZhIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6MTczNWZkZDQtMDk2Mi01ZTQ5LWEzZWEtMGY0NTBiMzc3ZDRmIj4gPHhtcE1NOkhpc3Rvcnk+IDxyZGY6U2VxPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iY3JlYXRlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDoxNzM1ZmRkNC0wOTYyLTVlNDktYTNlYS0wZjQ1MGIzNzdkNGYiIHN0RXZ0OndoZW49IjIwMjEtMDktMjVUMTI6MjE6NTctMDQ6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCAyMi40IChXaW5kb3dzKSIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6N2JjMzAxYmMtNTA0Ni01OTQzLWE2YzEtNTIwNGU5YWFjYjRlIiBzdEV2dDp3aGVuPSIyMDIxLTA5LTI2VDE1OjQyOjM0LTA0OjAwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgMjIuNCAoV2luZG93cykiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249InNhdmVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjllOWRiOWNhLWU5MjItZGI0Ny04Nzk4LTllMTMzMTRjYmU4YyIgc3RFdnQ6d2hlbj0iMjAyMS0wOS0yN1QxMTowMjozNC0wNDowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIDIyLjQgKFdpbmRvd3MpIiBzdEV2dDpjaGFuZ2VkPSIvIi8+IDwvcmRmOlNlcT4gPC94bXBNTTpIaXN0b3J5PiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Ps+Bv+gAABCTSURBVHja7d3ZcxRFAMfxX4xyRI4QPF5U8Aav8t3//9kqAa/y1gc8QxJBiZL1oWeFcQOGZHcz3fP5VHVplebY2a35Tu/2dJ6YTCYxDMMwDKPu8UQAgOoJOgAIOgAg6ACAoAMAgg4Agg4ACDoAIOgAgKADgKADAIIOAAg6ACDoACDoAICgAwCCDgAIOgAIOgAg6ACAoAMAgg4Agg4ACDoAIOgAgKADgKADAIIOAAg6ACDoACDoAICgAwCCDgAIOgAIOgAg6ACAoAMAgg4Agg4ACDoAIOgAgKADgKADAIIOAAg6ACDoAICgA4CgAwCCDgAIOgAg6AAg6ACAoAMAgg4ACDoACDoAIOgAgKADAIKeJCsrK8sa60v8WYZhGMZ/hqA3HvMlOZnkxSQvu2YE+NfVJB8kOZdk4SfksUXdW+5zfv0kuZDkSpKNJJdEHSDpzoXPJ3kqyZvdvyPog/V0kle7qE8vDUUdEPNyLnzwXPlaN1NH0AcZ80tJ1vb5b6IOiHnfk90E6BktEvQhuZDknSTPPuKYijog5n3nUz5X33CoBH0ITiR5IcnpA/y/og6Ied9qkpe6uGuSoB+Lle6q8p0kFx/j60QdEPO+symLiZ9z6AT9OJxKcjmHW9Qh6oCY9ydIp7tz6lmHUNCX6VzKYo4zR/geog6I+exE6fVupr7icAr6op1PeWtoHiszRR0Q89kJ05Uk6w6poC/SiZQd4Nbm+D1FHRDz2TZdTrmDaNXhFfR5mi6Ae7+bmc+bqANiPjtTv7qgc66gj3xm/tKcZ+aiDoj5oydSJ7rvv5S93wW9/Zn5esq+w+eX8PNEHRDzvrXY+13Q5+BMyorLjSVeHYo6IOZ9T3fn4vMOvaAf9qpw0W+zizog5gezmuSV2Ptd0B/ThSTvpuzNflyf24g6IOZ9D942jKD/r+mtaacH8LuIOiDmfU9252h7vwv6Q01vTXs7w/rLP6IOiHnf2ZSFcvZ+F/R9ne5etENcdCHqgJj3rXW/m73fBX3mau+VlJWUQyXqgJj3nYy93wX9AetJ3kodKydFHRDzvnMpb79vjP0JG3vQTyR5IcNYACfqgJgfzmrKbcbrGfHe72MN+nQB3Hup8/YHUQfEfHamfiXldmNBH5GT3Qv2TMWPQdQBMe9P1E51M/VRLpQbY9AvJHmjkSdc1AEx71vrzvGj2/t9jEG/nPJ2eyuPXdQBMe+b3rl0TtDb9mWS3cYek6gDYn7fbpIbSbYFvW3bSa6LOkCzMb8+tpiPNeiiDiDmgi7qog6IuZgLuqiLOiDmYi7ooi7qgJiLuaCLOiDmYi7ooi7qgJiLuaCLuqgDYi7mgi7qog6IuZgLuqiLOoi5mAu6qIs6IOZiLuiiLuqAmIu5oIu6qANiLuaCLuqiDmIu5oKOqANiLuaCLuqiDoi5mAu6qIs6IOYIuqiLOoi5mAs6og6IuZgLuqiLOiDmYi7ooi7qgJgj6KIu6iDmYi7oiLqog5iLuaCLuqgDYi7mgi7qog6IOYIu6qIOYi7mgo6oizqIuZgLOqIOiLmYC7qoizqIuZgj6KIu6iDmYi7oiLqog5iLuaAj6qIOYi7mgi7qog5iLuYIuqiLOoi5mAu6QyDqog5iLuaCjqiLOoi5mAs6og5iLuYIuqiLOoi5mCPooi7qIOZiLuiIuqiDmCPoiDqIuZgj6KIu6iDmYo6gi7qog5iLuaAj6qIOYo6gI+qijpiLOYIu6qIOYi7mCLqoizqIuZgLOqIu6iDmCDqiLuqIuZgj6Ig6iLmYI+iiLuog5mIu6Ii6qIOYI+iIuqgj5mKOoCPqIOZijqCLuqiDmIu5oCPqoo6YizmCjqiLOmIu5gg6oi7qiLmYI+iiLuog5mIu6Ii6qCPmYo6gI+qijpiLOYKOqIs6Yi7mCDqiDmIu5oKOqIs6Yi7mCDqiLuqIuZgj6Ii6qCPmYo6gI+qijpgj6Ii6qCPmYo6gI+qijpiLOYKOqIs6Yi7mCDqiLuqIOYIOoo6YizmCjqiLOmIu5gg6oi7qiLmYI+iIuqgj5gg6iDpiLuYIOqIu6oi5mCPoiLqoI+ZijqAj6qKOmIOgI+qiLuZijqAj6qKOmIs5go6oizpiLuYIOqIu6mIu5iDoiLqoi7mYI+gg6oi5mCPoiLqoI+ZijqAj6qIu5mIOgo6oi7qYizmCDqKOmIs5go6oizpiLuYIOqIu6mIu5iDoiLqoi7mYI+gg6qIu5mKOoCPqoo6Yg6Aj6qIu5mIOgo6oi7qYizmCDqIu6mIu5gg6iDpiDoKOqIu6mIs5CDqiLupiLuYIOoi6qIu5mCPoIOqiLuYg6Ii6qIu5mIOgI+qiLuZijqCDqIu6mIs5gg6iLupiDoIOoi7mYg6CjqiLupiLOYIOoi7qYi7mCDqI+lijLuYg6CDqYi7mIOiIuqiLuZgj6CDqoi7mYo6gg6iPNepiDoIOol551MUcBB1EvfKoizkIOoh65VEXcxB0EPXKoy7mIOgg6pVHXcxB0EHUK4+6mIOgg6hXHnUxB0EHUa886mIOgg6iXnnUxRwEHUS98qiLOQg6iHrlURdzEHQQ9cqjLuYg6CDqlUddzEHQQdQrj7qYg6CDqFcedTEHQQdRrzzqYg6CDqJeedTFHAQdRL3yqIs5CDqIeuVRF3MQdBD1yqMu5iDoIOqVR13MQdBB1BuIupiDoIOoI+Yg6CDqiDkIOog6Yg6CDqIu5iDogKiLOQg6iDpiDoIOoi7mYg6CDqIu5iDogKiLOQg6iDpiDoIOoi7mgKCDqIs5CDog6mIOgg6ijpiDoIOoizkg6CDqYg6CDoi6mIOgA6Iu5iDoIOpiDgg6iLqYg6ADoi7mIOiAqIs5CDqIupgDgg6iLuaAoIOoizkIOnCUqN9r8LHdE3MQdBiTi0lWG3xcq91jAwQdmvdykksNP75L3WMEBB3EXNQBQQcxF3UQdEDMRR0EHRBzUQdBBzEXdUDQQcxFHQQdEHNRB0EHxFzUQdBBzEUdEHQQc1EHQQfEXNRB0EHMxVzUQdBBzEVd1EHQQcxFHQTdIQAxF3UQdBBzRB0EHcQcUQdBBzEXdUDQQcxFHQQdxBxRB0EHMUfUQdBBzEUdEHQQc1EHQQcxR9RB0EHMl+yvbog6CDqIeaV2k1zrxq6og6CDmNcZ8+tJtrtxXdRB0EHM6435lKiDoIOYVx5zUQdBBzFvJOaiDoIOYt5IzEUdBB3EvJGYizoIOoh5IzEXdRB0EPNGYi7qIOgg5o3EXNRB0EHMG4m5qIOgg5g3EnNRB0EHMW8k5qIOgg5i3kjMRR0EHcS8kZiLOgg6iHkjMRd1EHQQ80ZiLuog6CDmjcRc1EHQQcwbibmog6CDmDcSc1EHQQcxbyTmog6CDmLeSMxFHQQdxLyRmIs6CDqIeSMxF3UQdBDzRmIu6iDoIOaNxFzUQdBBzBuJuaiDoIOYN0bUQdBBzEVd1EHQEXMxF3VRR9BBzMVc1EUdQQcxF3NRB0EHMRd1UQdBR8zFXNRFHUF3CBBzMRd1UUfQQczFXNRFHUFHzMVc1EUdBB0xF3NRF3UQdMRczEVd1BF0EHMxF3VRR9ARczFH1EHQEXMxF3VRB0FHzMVc1EUdQQcxF3NRF3UEHTEXc0QdBB0xF3NRF3UQdMRczEVd1BF0EHNEXdQRdMRczBF1UUfQEXMxF3VRB0FHzMVc1EUdQQcxR9RFHUFHzMUcURd1BB0xF3NEHQQdMRdzURd1BB3EHFEXdQQdMRdzRF3UEXTEXMwRdRB0xFzMRV3UEXQQc0Rd1BF0xFzMEXVRR9ARczFH1EUdQUfMxVzURR1BR8zFHFEXdQQdMRdzRF3UEXTEXMwRdVFH0BFzMUfUEXTEXMwRdVFH0BFzMUfURR1BR8zFHFEXdQQdMRdzRF3UBR0xF3NEXdQRdMRczBF1UUfQEXMxR9RFHUFHzEHURV3QEXMxB1FH0BFzMUfURR1BR8zFHFEXdQQdMQdRF3UEXczFHEQdQUfMxRxRF3UEHTEXc0Rd1BF0xBxEXdQRdDEXcxB1URd0xFzMEXVRR9ARczFH1EUdQUfMQdRFHUEXczEHURd1QUfMxRxEHUFHzMUcURd1BF3MxRxEXdQRdDEXcxB1URd0xFzMQdQRdMRczBF1UUfQxVzMQdRFHUEXczEHURd1QUfMxRxEXdQFHTEXc0Rd1BF0MRdzEHVRR9DFXMxB1EVd0BFzMQdRF3VBR8wBUUfQxVzMQdRFHUEXczEHURd1QUfMxRxEXdQFHTEHRF3UBV3MxRxEXdQRdDEXcxB1URd0xFzMQdRFXdARc0DURV3QxVzMAVFH0MVczEHURV3QEXMxB1EXdUEXczEHRF3UBV3MxRwQdQRdzMUcRF3UBR0xF3MQdVEXdDEXc0DURV3QxVzMAVEXdUEXczEHURd1QXcIxFzMQdRFXdDFXMwBURd1QRdzMQdEXdQFXczFHBB1BF3MAVEXdUEXczEHRF3UBV3MxRwQdVEXdDEXc0DUEXQxB0Rd1AVdzMUcEHVRF3QxF3NA1EVd0MVczAFRF3VBF3NA1EVd0MVczAFRF3VBF3MxB0Rd1AVdzMUcEPWDRv2qoLdtPcmZJBMxB2g66huC3rZbST7v/inmAG1G/XaSa4LevrtJvkvye5I9MQdoKup/J/k+yY6gj2em/mmSX8UcoJmobyX5KMnNtPXRqqA/wqSboX+T5A8xB6g+6ve6mfloz41j31jmdspn6rXM1MUcEPXZCdp2kk+S/DbmJ83Wr8lmks8qiKSYA6I+688kXyb5JXWvixL0Ocbym+4FvDfQ30/MAVGfjfm3GeECOEH//5n6EN+yEXNA1Pf/vW6kLIDb8zQJ+oMmKQvkvktyR8wBBhv1vSQ/mJkL+kFeuJ+lfB4j5gDDifok5da0a0l+9pQI+kFspax+P66ZupgDoj7rr5T1TpsZ4X3mgn60qH6RsgnNZMk/V8wBUe+7k7Ka3blR0A9lulBuU8wBji3qt1N29/wxZQMZBP2xTXJ/7/fbC56pizkg6rOmO8BZACfoc7HVXR0uaqGcmAOivv+5d7R7swv64mbqO0m+zvz3fhdzQNRnTf9q2pZDLOiLcCdl9fu8Np8Rc0DUZ+3M+Vwr6OxrM+Xt96NeNYo5IOr7T5y+SPJT7AAn6Euwm/v7B+8d8uvFHBD1vukiZAvgBH3pM/XD7P0u5oCoP/zr7M0u6Es3SXlr6NscfKGcmAOiPute7M0u6AOwk4Pt/S7mgKjPToy2knwce7ML+kDcSlmR+buYAxw46ndTbgf+Ne4zF/QB2U3yVXe1uSfmAI+M+p2UP7TibXZBH6TpQrlbYg7w0KjvdOfKm7E3u6AP1CTJn7m/u5GYA/SjfjcWwAn6kWs7WdpHNJtJPhRzgJmo30j5q2ktnfMF/biibhiGYRzb2F7WzzJDBwAEHQAQdABA0AFA0AEAQQcABB0AEHQAEHQAQNABAEEHAAQdABB0ABB0AEDQAQBBBwAEHQAEHQAQdABA0AEAQQcAQQcABB0AEHQAQNABQNABAEEHAAQdABB0ABB0AEDQAQBBBwAEHQAEHQAQdABA0AEAQQcAQQcABB0AEHQAQNABQNABAEEHAAQdABB0ABB0AEDQAQBBBwAEHQAEHQAQdABA0AEAQQcAQQcABB0AEHQAQNABAEEHgDb8A3Y0shv7YnzFAAAAAElFTkSuQmCC";

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ DragDrop)
/* harmony export */ });
/* harmony import */ var _fallback_png__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fallback.png */ "./src/fallback.png");
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.scss */ "./src/index.scss");
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_index_scss__WEBPACK_IMPORTED_MODULE_1__);



class DragDrop {
  constructor(itemContainer, dropzone, options) {
    this.mouseMove = this.mouseMove.bind(this);
    this.mouseDown = this.mouseDown.bind(this);
    this.mouseUp = this.mouseUp.bind(this);
    this.childrenChanged = this.childrenChanged.bind(this);
    this.update = this.update.bind(this);

    this.element = itemContainer;
    this.dropzone = dropzone;

    this.options = {
      preload: false, // if true, thumbnails will be preloaded instead of when needed
      selector: null,
      centered: true, // thumbnails will be centered to cursor
      offset: { x: 0, y: 0 },
      size: { width: 100, height: 100 },
      scrollMargin: 20,
      scrollAmount: 10,
      scrollInterval: 50,
      autoListen: true, // if false user must manually turn the listeners on and off with listen / unlisten methods
      disableContextMenu: false,
    };

    Object.assign(this.options, options);

    this.state = {
      dragging: false,
      current: null,
      position: { x: 0, y: 0 },
      listening: false,
    };

    this.thumbs = document.createElement('div');
    this.thumbs.className = 'dragdrop-thumbs';

    this.element.classList.add('dragdrop-container');

    this.observer = new MutationObserver(this.childrenChanged);
    this.observer.observe(this.element, { childList: true });

    if (this.options.autoListen) this.listen();
    this.update();
  }

  childrenChanged(changes) {
    if (!this.state.listening) return;

    changes.forEach((change) => {
      change.addedNodes.forEach((added) => {
        this.addItem(added);
      });

      change.removedNodes.forEach((removed) => {
        this.removeItem(removed);
      });
    });
  }

  listen() {
    this.state.listening = true;
    window.addEventListener('pointermove', this.mouseMove);
    window.addEventListener('pointerup', this.mouseUp);
    window.addEventListener('pointerleave', this.mouseUp);
    this.element.addEventListener('touchmove', this.touchMove);

    this.state.items = Array.from(this.element.children);
    this.state.items.forEach((item) => {
      this.addItem(item);
    });
  }

  unlisten() {
    this.state.listening = false;
    window.removeEventListener('pointermove', this.mouseMove);
    window.removeEventListener('pointerup', this.mouseUp);
    window.removeEventListener('pointerleave', this.mouseUp);
    this.element.removeEventListener('touchmove', this.touchMove);

    this.state.items = Array.from(this.element.children);
    this.state.items.forEach((item) => {
      this.removeItem(item);
    });
  }

  addItem(_item) {
    const item = this.options.selector ? _item.querySelector(this.options.selector) : _item;
    const thumb = item.getAttribute('data-thumb');
    if (!thumb) {
      console.warn(`ds-dragdrop warning: item ${item} missing 'data-thumb' attribute`);
    }
    item.addEventListener('dragstart', this.dragStart);
    item.addEventListener('pointerdown', this.mouseDown);
    if (thumb && this.options.preload) {
      const img = new Image();
      img.src = thumb;
    }
  }

  removeItem(_item) {
    const item = this.options.selector ? _item.querySelector(this.options.selector) : _item;
    item.removeEventListener('dragstart', this.dragStart);
    item.removeEventListener('pointerdown', this.mouseDown);
  }

  dragStart(e) {
    e.preventDefault();
  }

  mouseDown(e) {
    const path = e.path || (e.composedPath && e.composedPath());
    if (path[0].getAttribute('draggable') === 'false') return;
    // get target, making sure not to accidentally grab a child of the intended draggable item
    let { target } = e;
    while (target.parentNode !== this.element) {
      target = target.parentNode;
    }

    this.state.dragging = true;

    const thumb = new Image();
    thumb.style.width = `${this.options.size.width}px`;
    thumb.style.height = `${this.options.size.height}px`;
    thumb.src = target.getAttribute('data-thumb') || _fallback_png__WEBPACK_IMPORTED_MODULE_0__;
    thumb.className = 'dragdrop-thumb';
    thumb.setAttribute('draggable', false);
    if (this.options.disableContextMenu) {
      thumb.oncontextmenu = () => { return false };
    }
    document.body.appendChild(thumb);
    this.state.current = target;
    this.state.thumb = thumb;
    this.moveThumbToCursor(e);
    this.state.startDrag = true;
  }

  mouseMove(e) {
    if (this.state.startDrag) {
      if (this.state.current) this.element.dispatchEvent(new CustomEvent('dragged', { detail: this.state.current }));

      this.state.startDrag = false;
    }
    this.state.position.x = e.clientX;
    this.state.position.y = e.clientY;
    if (this.state.dragging) {
      this.moveThumbToCursor(e);
    }
  }

  touchMove(e) {
    e.preventDefault();
  }

  mouseUp(e) {
    this.state.dragging = false;
    const { left, top, width, height } = this.dropzone.getBoundingClientRect();

    const inDropZone = (e.clientY > top && e.clientY < top + height) && (e.clientX > left && e.clientX < left + width);

    if (this.state.current) {
      this.element.dispatchEvent(new CustomEvent('dropped', { detail: {
        inDropZone,
        element: this.state.current,
        position: this.state.position,
      } }));
      this.state.thumb.remove();
    }
    this.state.current = null;
  }

  moveThumbToCursor(e) {
    this.state.thumb.style.left = `${e.clientX + this.options.offset.x}px`;
    this.state.thumb.style.top = `${e.clientY + this.options.offset.y}px`;
  }


  update() {
    if (this.state.dragging) {
      if (this.state.position.y < this.options.scrollMargin) {
        window.scrollBy(0, -this.options.scrollAmount);
      }
      if (window.innerHeight - this.state.position.y < this.options.scrollMargin) {
        window.scrollBy(0, this.options.scrollAmount);
      }
    }
    setTimeout(this.update, this.options.scrollInterval);
  }
}

window.DragDrop = DragDrop;

})();

__webpack_exports__ = __webpack_exports__["default"];
/******/ 	return __webpack_exports__;
/******/ })()
;
});
//# sourceMappingURL=ds-dragdrop.js.map