# DS-DragDrop

Adds simulated drag / drop events to lists of items. Does not rely on the [drag and drop API](https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API) as it's not yet implemented across all desktop browsers or at all in mobile browsers.



### Usage:

`new DragDrop(container <HTMLElement>, dropzone <HTMLElement>, options <Object>);`

DS-DragDrop will assign special classes and listeners to all immediate children of `itemContainer` so that they can be dragged.

For custom thumbnails make sure each child of itemContainer has a `data-thumb` attribute, the value of each pointing to the thumbnail image you want to appear when dragging.   

**options.elector** *string*: if null, drag and drop listeners will be applied to the entire child, if not null drag and drop listeners will be applied to the first matching element with this selector. default is null. Note that this is also the element that ds-drag-drop will look for the 'data-thumb' attribute.  
**options.centered** *boolean*: true will center the thumbnail  
**options.preload** *boolean*: if true, all thumbs will be centered on the cursor, if false, the cursor's initial offset on the item will be maintained.  
**options.scrollMargin** *number*: if more than 0, this is the amount of pixels near the top and bottom edge that will force the page to scroll while dragging.  
**options.offset** *object*: contains x and y pixel values for where to attach the data-thumb image if centered isn't desired.

### Example:

```
import DragDrop from 'ds-dragdrop';

const itemContainer = document.querySelector('.items');
const dropZone = document.querySelector('.dropzone');

const dragdrop = new DragDrop(itemContainer, dropZone);

```

### Demo:

A demo project is included in this repo, to view it, serve the root of this folder (example: `serve`) and then view it in the browser at http://localhost:5000/demo


### Developing:


```
cd ~
git clone git@gitlab.com:daveseidman/drag-drop.git
cd ~/drag-drop
npm install  // <- install dependencies for ds-dragdrop
npm link     // <- creates a global symlink pointing to this folder
npm watch    // <- as you edit src/index.js webpack will automatically recompile the /dist folder
```

in another project use the symlinked version of ds-drag-drop by running the following command inside the project folder (at the same level as package.json):

`npm link ds-dragdrop`

Now as you make changes to the module your project should always have the latest version. If your project automatically rebuilds whenever one of it's dependencies are updated it should automatically update inside your project as well.
